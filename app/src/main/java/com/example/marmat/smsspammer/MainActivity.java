package com.example.marmat.smsspammer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private  Button send;
    private SmsManager smsmanager = SmsManager.getDefault();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        send = (Button) findViewById(R.id.button);
    }


    public void onClickSend(View v){
       EditText m = (EditText) findViewById(R.id.text);
       EditText phone = (EditText) findViewById(R.id.phone_number);
       EditText it = (EditText) findViewById(R.id.number_SMS);
       int imax = Integer.parseInt(it.getText().toString());
       String num = phone.getText().toString();
       String txt = m.getText().toString();
       CharSequence textsend = send.getText();
       for(int i=1; i<=imax; i++){
           send.setText(textsend.toString()+" "+i+"/"+imax);
           smsmanager.sendTextMessage( num, null, txt, null, null);
       }
    }
}
